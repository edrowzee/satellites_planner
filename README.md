# Satellites planner

## Getting started

```
git clone
cd folder
mvn clean install
cd target
java -jar .\satellites_planner-0.01-SNAPSHOT.jar -f ..\data\Facility2Constellation\ -f ..\data\Russia2Constellation2\
```

Tested on Windows 10:

openjdk version "11.0.15" 2022-04-19 LTS

OpenJDK Runtime Environment 18.9 (build 11.0.15+9-LTS)

OpenJDK 64-Bit Server VM 18.9 (build 11.0.15+9-LTS, mixed mode)

Apache Maven 3.9.2 
