package ru.khakhaton.generator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;

public abstract class AbstractIntermediateGenerator {

    protected static final Character CSV_DELIMITER = ',';
    protected static final String CHAPTER_ENCODING_1 = "Windows-1251";
    protected static final String MESSAGE_ERROR_CSV = "Error While writing CSV: ";

    protected void generateCsv(Path path, String filename, IntermediateGeneratorConsumer consumer) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             CSVPrinter printer = new CSVPrinter(
                     new OutputStreamWriter(out, CHAPTER_ENCODING_1),
                     CSVFormat.EXCEL.withDelimiter(CSV_DELIMITER))) {

            consumer.accept(printer);

            printer.flush();
            FileUtils.writeByteArrayToFile(
                    path.resolve(filename).toFile(),
                    out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(MESSAGE_ERROR_CSV, e);
        }
    }
}
