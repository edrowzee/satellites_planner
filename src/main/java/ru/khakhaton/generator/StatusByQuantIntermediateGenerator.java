package ru.khakhaton.generator;

import ru.khakhaton.entity.SatelliteEntity;

import java.nio.file.Path;
import java.util.List;

public class StatusByQuantIntermediateGenerator extends AbstractIntermediateGenerator {

    private static final String FILE_NAME = "all_satellites_status_by_quant.csv";

    public void generateFile(Path path, List<Byte[]> statusesByQuant, List<SatelliteEntity> satellites) {
        generateCsv(path, FILE_NAME, printer -> {
            Object[] header = new Object[satellites.size()];

            for (int i = 0; i < satellites.size(); i++) {
                header[i] = satellites.get(i).getId();
            }
            printer.printRecord(header);

            for (Byte[] statuses : statusesByQuant) {
                printer.printRecord(statuses);
            }
        });
    }
}
