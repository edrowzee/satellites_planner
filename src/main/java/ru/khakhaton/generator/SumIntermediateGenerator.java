package ru.khakhaton.generator;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

public class SumIntermediateGenerator extends AbstractIntermediateGenerator {

    private static final String FILE_NAME = "satellites_summary_data_by_quants.csv";

    public void generateFile(Path path, List<Double> sumList) {
        generateCsv(path, FILE_NAME, printer -> {
            for (Double sum : sumList) {
                printer.printRecord(ResultGenerator.formatData(Optional.ofNullable(sum).orElse(0D)));
            }
        });
    }
}
