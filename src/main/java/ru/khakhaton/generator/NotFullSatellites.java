package ru.khakhaton.generator;

import ru.khakhaton.entity.SatelliteEntity;

import java.nio.file.Path;
import java.util.List;

public class NotFullSatellites extends AbstractIntermediateGenerator {

    private static final String FILE_NAME = "not_full_satellites.csv";

    public void generateFile(Path path, double[] values, List<SatelliteEntity> satellites) {
        generateCsv(path, FILE_NAME, printer -> {
            for (int i = 0; i < satellites.size(); i++) {
                printer.printRecord(satellites.get(i).getId(), ResultGenerator.formatData(values[i]));
            }
        });
    }
}
