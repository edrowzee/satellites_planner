package ru.khakhaton.generator;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SumAmountIntermediateGenerator extends AbstractIntermediateGenerator {

    private static final String FILE_NAME = "all_satellites_storage_load_by_quants.csv";

    public void generateFile(Path path, List<Double[]> sumAmountList) {
        generateCsv(path, FILE_NAME, printer -> {
            for (Double[] sums : sumAmountList) {
                printer.printRecord(Arrays.stream(sums)
                        .map(ResultGenerator::formatData)
                        .collect(Collectors.toList()));
            }
        });
    }
}
