package ru.khakhaton.generator;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.data.util.Pair;
import ru.khakhaton.dto.*;
import ru.khakhaton.parser.AbstractParser;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ResultGenerator extends AbstractIntermediateGenerator {

    private static final String FILE_NAME = "all_satellites_data_transfer_by_day.csv";
    private static final String FILE_NAME_ALL_SUM = "sum_data_transfer_by_day.csv";
    private static final String INDENT_SPACE = " ";

    private final Map<String, List<ResultRowDto>> stationWithAccessMap = new HashMap<>();
    private final Map<String, List<ResultRowCameraDto>> cameraMap = new HashMap<>();

    public void addNewResultRow(ResultRowDto row, String station) {
        List<ResultRowDto> existingRowsByStation = stationWithAccessMap.get(station);
        ResultRowDto existingRow = existingRowsByStation != null
                ? existingRowsByStation.get(existingRowsByStation.size() - 1)
                : null;

        if (existingRow == null) {
            List<ResultRowDto> newRowsForStation = new ArrayList<>();
            row.setAccess(1);
            newRowsForStation.add(row);

            stationWithAccessMap.put(station, newRowsForStation);
        } else {
            if (existingRow.getSatelliteName().equals(row.getSatelliteName())
                    && existingRow.getEndTime().isEqual(row.getStartTime())) {
                existingRow.concatWithRow(row);
            } else {
                row.setAccess(existingRow.getAccess() + 1);
                existingRowsByStation.add(row);
            }
        }
    }

    public void addNewCameraRow(ResultRowCameraDto row, String kinosat) {
        List<ResultRowCameraDto> existingRowsByKinosat = cameraMap.get(kinosat);
        ResultRowCameraDto existingRow = existingRowsByKinosat != null
                ? existingRowsByKinosat.get(existingRowsByKinosat.size() - 1)
                : null;

        if (existingRow == null) {
            List<ResultRowCameraDto> newRowsForKinosat = new ArrayList<>();
            row.setAccess(1);
            newRowsForKinosat.add(row);

            cameraMap.put(kinosat, newRowsForKinosat);
        } else {
            if (!existingRow.isStopSum() && existingRow.getEndTime().isEqual(row.getStartTime()) && !row.isNewRow()) {
                existingRow.concatWithRow(row);
            } else {
                row.setAccess(existingRow.getAccess() + 1);
                existingRowsByKinosat.add(row);
            }
        }
    }

    public void generateGroundFiles(PathHolder pathHolder) {
        Path currentPath = pathHolder.getGroundPath();

        for (Map.Entry<String, List<ResultRowDto>> entry: stationWithAccessMap.entrySet()) {
            try(BufferedWriter writer =
                        Files.newBufferedWriter(currentPath.resolve(generateStationNameFile(entry.getKey())))) {

                writer.append(removeFacilityFromStation(entry.getKey())).append(System.lineSeparator());
                writer.append("-------------------------").append(System.lineSeparator());
                writer.append("Access * Start Time (UTCG) * Stop Time (UTCG) * Duration (sec) * Sat" +
                        "name * Data (Mbytes)").append(System.lineSeparator());

                for (ResultRowDto row : entry.getValue()) {
                    writer.append(formatString(row.getAccess() + ".", 7)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getStartTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getEndTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(formatDuration(row.getDuration()), 8)).append(INDENT_SPACE);
                    writer.append(formatString(removeSatelliteFromName(row.getSatelliteName()), 14))
                            .append(INDENT_SPACE);
                    writer.append(formatString(formatData(row.getData() * 1024D), 16)).append(System.lineSeparator());
                }

                writer.flush();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void generateCameraFiles(PathHolder pathHolder) {
        Path currentPath = pathHolder.getCameraPath();

        for (Map.Entry<String, List<ResultRowCameraDto>> entry: cameraMap.entrySet()) {
            try(BufferedWriter writer =
                        Files.newBufferedWriter(currentPath.resolve(generateCameraNameFile(entry.getKey())))) {

                writer.append(entry.getKey()).append(System.lineSeparator());
                writer.append("-------------------------").append(System.lineSeparator());
                writer.append("Access * Start Time (UTCG) * Stop Time (UTCG) * Duration (sec)" +
                        " * Data (Mbytes)").append(System.lineSeparator());

                for (ResultRowCameraDto row : entry.getValue()) {
                    writer.append(formatString(row.getAccess() + ".", 7)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getStartTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getEndTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(formatDuration(row.getDuration()), 8)).append(INDENT_SPACE);
                    writer.append(formatString(formatData(row.getData() * 1024D), 16)).append(System.lineSeparator());
                }

                writer.flush();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void generateDropFiles(PathHolder pathHolder) {
        Path currentPath = pathHolder.getDropPath();
        Map<String, List<ResultRowKinosatDto>> map = new HashMap<>();

        for (Map.Entry<String, List<ResultRowDto>> entry: stationWithAccessMap.entrySet()) {
            for (ResultRowDto row : entry.getValue()) {
                if (map.containsKey(row.getSatelliteName())) {
                    map.get(row.getSatelliteName()).add(createResultRowKinosatDto(row, entry.getKey()));
                } else {
                    List<ResultRowKinosatDto> list = new ArrayList<>();

                    list.add(createResultRowKinosatDto(row, entry.getKey()));

                    map.put(row.getSatelliteName(), list);
                }
            }
        }

        for (Map.Entry<String, List<ResultRowKinosatDto>> entry: map.entrySet()) {
            try(BufferedWriter writer =
                        Files.newBufferedWriter(currentPath.resolve(generateKinosatNameFile(entry.getKey())))) {

                writer.append(entry.getKey()).append(System.lineSeparator());
                writer.append("-------------------------").append(System.lineSeparator());
                writer.append("Access * Start Time (UTCG) * Stop Time (UTCG) * Duration (sec) * Station" +
                        " * Data (Mbytes)").append(System.lineSeparator());

                List<ResultRowKinosatDto> sortedList = entry.getValue().stream()
                        .sorted(Comparator.comparing(ResultRowKinosatDto::getStartTime))
                        .collect(Collectors.toList());

                int i = 1;

                for (ResultRowKinosatDto row : sortedList) {
                    writer.append(formatString(i + ".", 7)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getStartTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(AbstractParser.DATE_TIME_FORMATTER
                            .format(row.getEndTime()), 26)).append(INDENT_SPACE);
                    writer.append(formatString(formatDuration(row.getDuration()), 8)).append(INDENT_SPACE);
                    writer.append(formatString(removeFacilityFromStation(row.getStationName()), 14))
                            .append(INDENT_SPACE);
                    writer.append(formatString(formatData(row.getData() * 1024D), 16)).append(System.lineSeparator());
                    ++i;
                }

                writer.flush();

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private ResultRowKinosatDto createResultRowKinosatDto(ResultRowDto row, String station) {
        return ResultRowKinosatDto.builder()
                .access(row.getAccess())
                .startTime(row.getStartTime())
                .endTime(row.getEndTime())
                .stationName(station)
                .data(row.getData())
                .build();
    }

    private String generateStationNameFile(String station) {
        return "Ground_" + removeFacilityFromStation(station) + ".txt";
    }

    private String generateCameraNameFile(String kinosat) {
        return "Camera_" + kinosat + ".txt";
    }

    private String removeFacilityFromStation(String station) {
        return station.split("Facility-")[1];
    }

    private String generateKinosatNameFile(String kinosat) {
        return "Drop_" + kinosat + ".txt";
    }

    public void generateTransferByDayFile(Path path) {
        Map<Pair<String, LocalDate>, Double> map = new HashMap<>();
        Map<LocalDate, Double> sumMap = new HashMap<>();

        for (Map.Entry<String, List<ResultRowDto>> entry: stationWithAccessMap.entrySet()) {
            for (ResultRowDto row : entry.getValue()) {
                LocalDate currentDate = row.getStartTime().toLocalDate();
                String satellite = row.getSatelliteName();
                Pair<String, LocalDate> currentPair = Pair.of(satellite, currentDate);

                map.merge(currentPair, row.getData(), Double::sum);
                sumMap.merge(currentDate, row.getData(), Double::sum);
            }
        }
        List<LocalDate> allDistinctDates = map.keySet().stream()
                .map(Pair::getSecond)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        List<LocalDate> allDistinctDatesInSumMap = sumMap.keySet().stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        List<String> allDistinctSatellites = map.keySet().stream()
                .map(Pair::getFirst)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        generateCsv(path, FILE_NAME, printer -> {
            Object[] satellitesHeader = new Object[allDistinctSatellites.size() + 1];
            satellitesHeader[0] = "Date";

            for (int i = 1; i < satellitesHeader.length; i++) {
                satellitesHeader[i] = allDistinctSatellites.get(i - 1);
            }
            printer.printRecord(satellitesHeader);

            for (int i = 0; i < allDistinctDates.size(); i++) {
                Object[] datesWithData = new Object[allDistinctSatellites.size() + 1];

                datesWithData[0] = allDistinctDates.get(i);
                for (int j = 0; j < allDistinctSatellites.size(); j++) {
                    Double data = map.get(Pair.of(allDistinctSatellites.get(j), allDistinctDates.get(i)));
                    datesWithData[j + 1] = ResultGenerator.formatData(data == null ? 0D : data * 1024D);
                }
                printer.printRecord(datesWithData);
            }
        });

        generateCsv(path, FILE_NAME_ALL_SUM, printer -> {
            printer.printRecord("Date", "Transferred sum");

            for (LocalDate date : allDistinctDatesInSumMap) {
                Double data = sumMap.get(date);

                printer.printRecord(date, formatData(data == null ? 0D : data * 1024D));
            }
        });
    }

    public void generateReportFile(Path path, ReportDto report, RuntimeInfo runtimeInfo) {
        try(BufferedWriter writer =
                    Files.newBufferedWriter(path.resolve("report.txt"))) {

            writer.append("quant: ").append(String.valueOf(report.getQuant())).append(System.lineSeparator());
            writer.append("principle: ").append(String.valueOf(report.getPrinciple())).append(System.lineSeparator());
            writer.append("countDays: ").append(String.valueOf(report.getCountDays())).append(System.lineSeparator());
            writer.append("averageLoadInGb: ").append(formatData(report.getAverageLoadInGb())).append(System.lineSeparator());
            writer.append("sumTransferInGb: ").append(formatData(report.getSumInGb())).append(System.lineSeparator());
            writer.append("minutesDuration: ").append(formatData(report.getMinutesDuration())).append(System.lineSeparator());
            writer.append("fullSatellitesMinutes: ").append(formatData(report.getFullSatellitesMinutes())).append(System.lineSeparator());
            writer.append("stationsActivePercent: ").append(formatData(report.getStationsActivePercent())).append(System.lineSeparator());
            writer.append("Parsing executing time: ").append(
                    prettyTimeDuration(runtimeInfo.getParsingEnd() - runtimeInfo.getStartProgram()))
                    .append(System.lineSeparator());
            writer.append("Algorithm executing time: ").append(
                    prettyTimeDuration(runtimeInfo.getAlgorithmEnd() - runtimeInfo.getParsingEnd()))
                    .append(System.lineSeparator());
            writer.append("Program executing time: ").append(
                    prettyTimeDuration(runtimeInfo.getProgramEnd() - runtimeInfo.getStartProgram()))
                    .append(System.lineSeparator());

            writer.flush();

        }  catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String prettyTimeDuration(long millis) {
        return DurationFormatUtils.formatDurationWords(millis, true, false);
    }

    public String formatDuration(long duration) {
        long minutes = duration / 1000;
        long millis = duration % 1000;

        StringBuilder millisString = new StringBuilder(String.valueOf(millis));

        for (int i = millisString.length(); i < 3; i++) {
            millisString.insert(0, "0");
        }
        return minutes + "." + millisString;
    }

    private String removeSatelliteFromName(String name) {
        String[] strings = name.split("Satellite-");

        return strings.length == 1 ? name : strings[1];
    }

    public static String formatData(double data) {
        DecimalFormat format = new DecimalFormat("0.0000");
        return format.format(BigDecimal.valueOf(data).setScale(4, BigDecimal.ROUND_HALF_UP)).replace(',', '.');
    }

    private String formatString(String value, int numberOfSpaces) {
        StringBuilder builder = new StringBuilder();

        for (int i = value.length(); i < numberOfSpaces; i++) {
            builder.append(" ");
        }
        return builder.append(value).toString();
    }
}
