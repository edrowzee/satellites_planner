package ru.khakhaton.generator;

import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;

public interface IntermediateGeneratorConsumer {

    void accept(CSVPrinter printer) throws IOException;
}
