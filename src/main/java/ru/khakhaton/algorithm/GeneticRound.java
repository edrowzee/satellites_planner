package ru.khakhaton.algorithm;

import lombok.Getter;
import ru.khakhaton.dto.CalcSumVectorDto;
import ru.khakhaton.dto.ResultRowCameraDto;
import ru.khakhaton.dto.ResultRowDto;
import ru.khakhaton.entity.FlyOverRussiaEntity;
import ru.khakhaton.entity.FlyOverStationEntity;
import ru.khakhaton.entity.SatelliteEntity;
import ru.khakhaton.entity.SatelliteTypeEntity;
import ru.khakhaton.generator.ResultGenerator;
import ru.khakhaton.repository.FlyOverRussiaRepository;
import ru.khakhaton.repository.FlyOverStationRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GeneticRound {

    private FlyOverStationRepository flyOverStationRepository;
    private FlyOverRussiaRepository flyOverRussiaRepository;
    private Random randomGenerator = new Random();

    private int[] ad;
    @Getter
    private int[] ac;
    @Getter
    private int[] d;
    private boolean[] flagsForCamera;
    private double[] prev_sat_data_vector;
    private LocalDateTime cur_time;
    private List<SatelliteEntity> satellites_list;
    private List<String> satellites_name_list;
    private int t_quant;
    private long[] max_data_capacity_vector;
    private long[] download_speed_vector;
    private long[] capture_speed_vector;
    private List<FlyOverStationEntity> station_flyovers;
    private ResultGenerator resultGenerator;

    public GeneticRound(FlyOverStationRepository flyOverStationRepository, FlyOverRussiaRepository flyOverRussiaRepository,
                        double[] prev_sat_data_vector, LocalDateTime cur_time,
                        List<SatelliteEntity> satellites_list, int t_quant, ResultGenerator resultGenerator) {
        this.resultGenerator = resultGenerator;
        this.flyOverStationRepository = flyOverStationRepository;
        this.flyOverRussiaRepository = flyOverRussiaRepository;
        this.prev_sat_data_vector = prev_sat_data_vector;
        this.cur_time = cur_time;
        this.satellites_list = satellites_list;
        this.satellites_name_list = satellites_list.stream().map(SatelliteEntity::getId).collect(Collectors.toList());
        this.t_quant = t_quant;
        this.ad = new int[prev_sat_data_vector.length];
        this.ac = new int[prev_sat_data_vector.length];
        this.d = new int[prev_sat_data_vector.length];
        this.flagsForCamera = new boolean[prev_sat_data_vector.length];
        this.max_data_capacity_vector = new long[satellites_list.size()];
        this.download_speed_vector = new long[satellites_list.size()];
        this.capture_speed_vector = new long[satellites_list.size()];
        for (int i = 0; i < satellites_list.size(); i++) {
            SatelliteTypeEntity type = satellites_list.get(i).getSatelliteType();
            max_data_capacity_vector[i] = (long) (type.getMemory().doubleValue() * 8 * 1024L * 1024 * 1024 * 1024);
            download_speed_vector[i] = (long) (type.getOutputSpeed().doubleValue() * 1024 * 1024 * 1024);
            capture_speed_vector[i] = (long) (type.getInputSpeed().doubleValue() * 1024 * 1024 * 1024);
        }
    }

    public void fill_d_vector(int principle) {
        Set<String> active_stations_set = station_flyovers.stream()
                .map(FlyOverStationEntity::getStation)
                .collect(Collectors.toSet());
        SortedMap<String, List<String>> satellites_to_station_dict = new TreeMap<>(active_stations_set.stream()
                .collect(Collectors.toMap(Function.identity(), tmp -> new ArrayList<>())));

        for (FlyOverStationEntity flyOverStation : station_flyovers) {
            int index = satellites_name_list.indexOf(flyOverStation.getSatellite());

            if (ad[index] > 0) {
                satellites_to_station_dict.get(flyOverStation.getStation()).add(flyOverStation.getSatellite());
            }
        }
        for (Map.Entry<String, List<String>> entry: satellites_to_station_dict.entrySet()) {
            satellites_to_station_dict.put(entry.getKey(), entry.getValue().stream().sorted().collect(Collectors.toList()));
        }
        Map<String, Integer> satellites_countTmp = new HashMap<>();

        for (Map.Entry<String, List<String>> entry: satellites_to_station_dict.entrySet()) {
            if (!entry.getValue().isEmpty()) {
                satellites_countTmp.put(entry.getKey(), entry.getValue().size());
            }
        }
        SortedMap<String, Integer> satellites_count = new TreeMap<>(satellites_countTmp);
        Map<String, String> chosen_sat_station_dict = new HashMap<>();

        List<String> satellites_d_list = new ArrayList<>();

        while (!satellites_count.isEmpty()) {
            // TODO random if many
            Map.Entry<String, Integer> entry = Collections.min(satellites_count.entrySet(), Map.Entry.comparingByValue());
            Map<String, Integer> res = new HashMap<>();
            for (Map.Entry<String, Integer> entryTmp: satellites_count.entrySet()) {
                if (entryTmp.getValue().equals(entry.getValue())) {
                    res.put(entryTmp.getKey(), entryTmp.getValue());
                }
            }

            String station = res.keySet().stream()
                    .collect(Collectors.toList())
                    .stream()
                    .sorted()
                    .collect(Collectors.toList())
                    .get(0);

            String remove_satellite;

            switch (principle) {
                case 0:
                    remove_satellite = getFirstSatellite(satellites_to_station_dict, station);
                    break;
                case 1:
                    remove_satellite = satellites_to_station_dict.get(station).stream()
                            .sorted()
                            .collect(Collectors.toList()).get(0);
                    for (String sat : satellites_to_station_dict.get(station).stream()
                            .sorted()
                            .collect(Collectors.toList())) {
                        double cur_sat_percent = get_satellite_load_percent(sat);
                        double prev_sat_percent = get_satellite_load_percent(remove_satellite);
                        if (cur_sat_percent > prev_sat_percent) {
                            remove_satellite = sat;
                        }
                    }
                    break;
                case 2:
                    remove_satellite = getFirstSatellite(satellites_to_station_dict, station);
                    break;
                case 3:
                    List<String> satIterator1 = satellites_to_station_dict.get(station).stream()
                            .sorted()
                            .collect(Collectors.toList());
                    remove_satellite = satIterator1.get(0);
                    for (String sat : satIterator1) {
                        int indexOfSat = satellites_name_list.indexOf(sat);
                        int indexOfRemoveSatellite = satellites_name_list.indexOf(remove_satellite);

                        double cur_sat_data = prev_sat_data_vector[indexOfSat];
                        double prev_sat_data = prev_sat_data_vector[indexOfRemoveSatellite];
                        double v_cur = satellites_list.get(indexOfSat).getSatelliteType().getOutputSpeed().doubleValue();
                        double v_prev = satellites_list.get(indexOfRemoveSatellite).getSatelliteType().getOutputSpeed().doubleValue();
                        double s_max_cur = satellites_list.get(indexOfSat).getSatelliteType().getMemory().doubleValue();
                        double s_max_prev = satellites_list.get(indexOfRemoveSatellite).getSatelliteType().getMemory().doubleValue();
                        double i_max_cur = satellites_list.get(indexOfSat).getSatelliteType().getInputSpeed().doubleValue();
                        double i_max_prev = satellites_list.get(indexOfRemoveSatellite).getSatelliteType().getInputSpeed().doubleValue();
                        double coeff = (v_cur * s_max_cur * i_max_cur) / (v_prev * s_max_prev * i_max_prev);

                        if (cur_sat_data > coeff * prev_sat_data) {
                            remove_satellite = sat;
                        }
                    }
                    break;
                default:
                    remove_satellite = getFirstSatellite(satellites_to_station_dict, station);
            }
            satellites_to_station_dict.remove(station);
            satellites_count.remove(station);

            chosen_sat_station_dict.put(station, remove_satellite);

            for (Map.Entry<String, List<String>> entrySecond : satellites_to_station_dict.entrySet()) {
                if (entrySecond.getValue().contains(remove_satellite)) {
                    satellites_to_station_dict.get(entrySecond.getKey()).remove(remove_satellite);
                    Integer tmp = satellites_count.get(entrySecond.getKey());

                    if (tmp - 1 == 0) {
                        satellites_count.remove(entrySecond.getKey());
                    } else {
                        satellites_count.put(entrySecond.getKey(), tmp - 1);
                    }
                }
            }

            satellites_d_list.add(remove_satellite);
        }
        for (Map.Entry<String, String> entry: chosen_sat_station_dict.entrySet()) {
            for (FlyOverStationEntity flyover : station_flyovers) {
                if (entry.getKey().equals(flyover.getStation()) && (entry.getValue().equals(flyover.getSatellite()))) {
                    int cur_sat_d = satellites_name_list.indexOf(entry.getValue());

                    if (cur_time.plusSeconds(t_quant).isBefore(flyover.getEndTime())
                            || cur_time.plusSeconds(t_quant).isEqual(flyover.getEndTime())) {
                        d[cur_sat_d] = t_quant * 1000;
                    } else {
                        d[cur_sat_d] = (int) Duration.between(cur_time, flyover.getEndTime()).toMillis();
                    }
                    if (prev_sat_data_vector[cur_sat_d] - (d[cur_sat_d] * download_speed_vector[cur_sat_d]) / 1000D < 0) {
                        d[cur_sat_d] = (int) (1000 * prev_sat_data_vector[cur_sat_d] / download_speed_vector[cur_sat_d]);
                    }

                    LocalDateTime stopTime = cur_time.plus(Duration.ofMillis(d[cur_sat_d]));
                    ResultRowDto row = ResultRowDto.builder()
                            .startTime(cur_time)
                            .endTime(stopTime)
                            .satelliteName(flyover.getSatellite())
                            .build();

                    row.setData((row.getDuration() / 1000D)
                            * satellites_list.get(cur_sat_d).getSatelliteType().getOutputSpeed().doubleValue()
                            / 8D);

                    resultGenerator.addNewResultRow(row, entry.getKey());
                }
            }
        }
    }

    private String getFirstSatellite(SortedMap<String, List<String>> satellites_to_station_dict, String station) {
        return satellites_to_station_dict.get(station).stream()
                .sorted()
                .collect(Collectors.toList()).get(0);
    }

    public CalcSumVectorDto calc_sum_vector() {
        double[] delta_sum_cur_step = new double[satellites_list.size()];
        for (int i = 0; i < satellites_list.size(); i++) {
            delta_sum_cur_step[i] = ((capture_speed_vector[i] * ac[i])
                    - (download_speed_vector[i] * d[i])) / 1000D;
        }

        double[] downloaded_cur_step = new double[satellites_list.size()];
        for (int i = 0; i < satellites_list.size(); i++) {
            downloaded_cur_step[i] = prev_sat_data_vector[i] - ((download_speed_vector[i] * d[i]) / 1000D);
        }
        for (int i = 0; i < satellites_list.size(); i++) {
            if (downloaded_cur_step[i] >= 0) {

            } else {
                downloaded_cur_step[i] = 0D;
            }
        }
        for (int i = 0; i < satellites_list.size(); i++) {
            downloaded_cur_step[i] = prev_sat_data_vector[i] - downloaded_cur_step[i];
        }

        double[] sum_cur_step = new double[satellites_list.size()];
        for (int i = 0; i < satellites_list.size(); i++) {
            sum_cur_step[i] = delta_sum_cur_step[i] + prev_sat_data_vector[i];
        }
        for (int i = 0; i < satellites_list.size(); i++) {
            if (sum_cur_step[i] <= max_data_capacity_vector[i]) {
            } else {
                sum_cur_step[i] = max_data_capacity_vector[i];
            }
        }

        long[] full_satellites_now_duration = new long[satellites_list.size()];
        for (int i = 0; i < satellites_list.size(); i++) {
            if (sum_cur_step[i] != max_data_capacity_vector[i]) {
                full_satellites_now_duration[i] = 0L;
            } else {
                full_satellites_now_duration[i] = t_quant * 1000L - d[i] - ac[i];
            }
        }

        for (int i = 0; i < satellites_list.size(); i++) {
            if (sum_cur_step[i] >= 0) {
            } else {
                sum_cur_step[i] = 0L;
            }
        }
        return CalcSumVectorDto.builder()
                .sum_cur_step(sum_cur_step)
                .downloaded_cur_step(downloaded_cur_step)
                .stations_active(Arrays.stream(d).filter(value -> value != 0).count())
                .full_satellites_list_duration_sum(full_satellites_now_duration)
                .build();
    }

    public void fill_vectors_optimized(List<FlyOverStationEntity> station_flyovers_raw,
                                        List<FlyOverRussiaEntity> country_flyovers_raw,
                                       int principle) {
        fill_ad_vector_optimized(station_flyovers_raw);
        fill_d_vector(principle);
        fill_ac_vector_optimized(country_flyovers_raw);
    }


    public void fill_ad_vector_optimized(List<FlyOverStationEntity> station_flyovers_raw) {
        station_flyovers = new ArrayList<>();
        for (FlyOverStationEntity flyover : station_flyovers_raw) {

            if ((cur_time.isAfter(flyover.getStartTime()) || cur_time.isEqual(flyover.getStartTime()))
                    && (cur_time.isBefore(flyover.getEndTime()))) {
                station_flyovers.add(flyover);
            }
        }

        for (FlyOverStationEntity flyover: station_flyovers){
            for (int j=0; j < satellites_list.size(); j++){
                if (satellites_list.get(j).getId().equals(flyover.getSatellite())) {
                    ad[j] = 1;
                }
            }
        }
        for (int i = 0; i < prev_sat_data_vector.length; i++) {
            if (prev_sat_data_vector[i] > 0) {

            } else {
                ad[i] = 0;
            }
        }
    }

    private void fill_ac_vector_optimized(List<FlyOverRussiaEntity> country_flyovers_raw) {
        List<FlyOverRussiaEntity> country_flyovers = new ArrayList<>();

        for (FlyOverRussiaEntity flyover : country_flyovers_raw) {

            if ((cur_time.isAfter(flyover.getStartTime()) || cur_time.isEqual(flyover.getStartTime()))
                    && (cur_time.isBefore(flyover.getEndTime()))) {
                country_flyovers.add(flyover);
            }
        }

        for (FlyOverRussiaEntity flyover: country_flyovers){
            int cur_sat_ac = satellites_name_list.indexOf(flyover.getSatellite());
            if (d[cur_sat_ac] == t_quant * 1000) {
                continue;
            }

            if (cur_time.plusSeconds(t_quant).isBefore(flyover.getEndTime()) ||
                    cur_time.plusSeconds(t_quant).isEqual(flyover.getEndTime())) {
                ac[cur_sat_ac] = t_quant * 1000;
            } else {
                ac[cur_sat_ac] = (int) (Duration.between(cur_time, flyover.getEndTime()).toMillis());
            }

            double delta_ac_data = (ac[cur_sat_ac] * capture_speed_vector[cur_sat_ac]) / 1000D;
            double delta_d_data = (d[cur_sat_ac] * download_speed_vector[cur_sat_ac]) / 1000D;
            double tmp3 = (double) prev_sat_data_vector[cur_sat_ac];
            double tmp4 = (double) max_data_capacity_vector[cur_sat_ac];

            if (delta_ac_data - delta_d_data + tmp3 > tmp4) {
                double tmp = ((double) max_data_capacity_vector[cur_sat_ac]) -
                        ((double) prev_sat_data_vector[cur_sat_ac]) - delta_d_data;
                double tmp2 = (double) capture_speed_vector[cur_sat_ac];
                ac[cur_sat_ac] = (int) (tmp / tmp2 * 1000D);
            }
            ac[cur_sat_ac] = Math.max(ac[cur_sat_ac] - d[cur_sat_ac], 0);
        }
        for (int i = 0; i < prev_sat_data_vector.length; i++) {
            if (ac[i] > 0 && d[i] > 0) {
                flagsForCamera[i] = true;
            }
        }

        for (int i = 0; i < satellites_name_list.size(); i++) {
            if (ac[i] > 0) {
                if (flagsForCamera[i]) {
                    generateResultRowCameraDtoNew(i);
                } else {
                    generateResultRowCameraDtoOld(i);
                }
            }
        }
    }

    private void generateResultRowCameraDtoOld(int i) {
        ResultRowCameraDto cameraRow = ResultRowCameraDto.builder()
                .startTime(cur_time)
                .endTime(cur_time.plus(Duration.ofMillis(ac[i])))
                .stopSum(ac[i] != t_quant * 1000)
                .build();

        cameraRow.setData((cameraRow.getDuration() / 1000D)
                * satellites_list.get(i).getSatelliteType().getInputSpeed().doubleValue()
                / 8D);

        resultGenerator.addNewCameraRow(cameraRow, satellites_name_list.get(i));
    }

    private void generateResultRowCameraDtoNew(int i) {
        ResultRowCameraDto cameraRow = ResultRowCameraDto.builder()
                .startTime(cur_time.plus(Duration.ofMillis(d[i])))
                .endTime(cur_time.plus(Duration.ofMillis(d[i]+ac[i])))
                .newRow(true)
                .build();

        cameraRow.setData((cameraRow.getDuration() / 1000D)
                * satellites_list.get(i).getSatelliteType().getInputSpeed().doubleValue()
                / 8D);

        resultGenerator.addNewCameraRow(cameraRow, satellites_name_list.get(i));
    }

    private double get_satellite_load_percent(String sat_name) {
        double cur_data_amount = (double) prev_sat_data_vector[satellites_name_list.indexOf(sat_name)];
        double max_data_amount = (double) max_data_capacity_vector[satellites_name_list.indexOf(sat_name)];
        return (double) (100 * (cur_data_amount / max_data_amount));
    }
}
