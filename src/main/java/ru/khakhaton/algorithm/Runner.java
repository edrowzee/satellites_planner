package ru.khakhaton.algorithm;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.khakhaton.dto.CalcSumVectorDto;
import ru.khakhaton.dto.PathHolder;
import ru.khakhaton.dto.ReportDto;
import ru.khakhaton.dto.RuntimeInfo;
import ru.khakhaton.entity.FlyOverRussiaEntity;
import ru.khakhaton.entity.FlyOverStationEntity;
import ru.khakhaton.entity.SatelliteEntity;
import ru.khakhaton.generator.*;
import ru.khakhaton.repository.FlyOverRussiaRepository;
import ru.khakhaton.repository.FlyOverStationRepository;
import ru.khakhaton.repository.SatelliteRepository;
import ru.khakhaton.service.FileService;
import ru.khakhaton.util.LocalDateTimeUtils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class Runner {

    private final SatelliteRepository satelliteRepository;
    private final FlyOverRussiaRepository flyOverRussiaRepository;
    private final FlyOverStationRepository flyOverStationRepository;

    public void start(int quant, int d_principle, int countDays, RuntimeInfo runtimeInfo) {
        List<SatelliteEntity> satellites = satelliteRepository.findAllWithJoinType();
        int satellites_cnt = satellites.size();
        LocalDateTime min_start_time_over_russia = flyOverRussiaRepository.findMinStartTime();
        LocalDateTime min_start_time_over_station = flyOverStationRepository.findMinStartTime();
        LocalDateTime min_start_time = Stream.of(min_start_time_over_russia, min_start_time_over_station)
                .min(LocalDateTime::compareTo)
                .orElse(null);
        LocalDateTime max_end_time_over_russia = flyOverRussiaRepository.findMaxEndTime();
        LocalDateTime max_end_time_over_station = flyOverStationRepository.findMaxEndTime();
        LocalDateTime max_end_time = Stream.of(max_end_time_over_russia, max_end_time_over_station)
                .min(LocalDateTime::compareTo)
                .orElse(null);
        int daysAll = (int) Duration.between(min_start_time, max_end_time).toDays();
        List<Double> sum_list = new ArrayList<>();
        List<Double[]> sat_data_amount_list = new ArrayList<>();
        double[] sat_data_amount = new double[satellites_cnt];
        int window_size = 600 - (600 % quant);
        int quant_steps = 24 * 60 * 60 * countDays;
        LocalDateTime window_start_time = min_start_time;
        LocalDateTime window_end_time = min_start_time.plusSeconds(window_size);
        long countDistinctStations = flyOverStationRepository.countDistinctStations();

        ResultGenerator resultGenerator = new ResultGenerator();

        List<FlyOverStationEntity> window_station_flyovers = null;
        List<FlyOverRussiaEntity> window_country_flyovers = null;
        int window_counter = 0;
        int fullSatellitesQuants = 0;
        List<Double> full_satellites_list = new ArrayList<>();
        List<Double> downloaded_data_list = new ArrayList<>();
        long[] full_satellites_list_duration_sum = new long[satellites_cnt];
        double[] not_full_satellites_list_duration_double = new double[satellites_cnt];
        List<Byte[]> satelliteStatuses = new ArrayList<>();
        long stations_active_sum = 0;
        for (int j = 0; j < quant_steps; j += quant) {
            if (window_counter == 0) {
                window_station_flyovers = flyOverStationRepository.findByDates(
                        LocalDateTimeUtils.stripMillis(window_start_time), LocalDateTimeUtils.stripMillis(window_end_time));
                window_country_flyovers = flyOverRussiaRepository.findByDates(
                        LocalDateTimeUtils.stripMillis(window_start_time), LocalDateTimeUtils.stripMillis(window_end_time));
            }

            LocalDateTime cur_time = window_start_time.plusSeconds(window_counter);
            if (window_counter == window_size) {
                window_counter = 0;
                window_start_time = window_start_time.plusSeconds(window_size);
                window_end_time = window_end_time.plusSeconds(window_size);
            } else {
                window_counter += quant;
            }

            GeneticRound genetic_object = new GeneticRound(flyOverStationRepository, flyOverRussiaRepository,
                    sat_data_amount, cur_time, satellites, quant, resultGenerator);

            genetic_object.fill_vectors_optimized(window_station_flyovers, window_country_flyovers, d_principle);
            CalcSumVectorDto dto = genetic_object.calc_sum_vector();

            for (int i = 0; i < satellites_cnt; i++) {
                full_satellites_list_duration_sum[i] += dto.getFull_satellites_list_duration_sum()[i];
            }
             Arrays.stream(genetic_object.getD()).sum();

            sat_data_amount = dto.getSum_cur_step();
            downloaded_data_list.add(Arrays.stream(dto.getDownloaded_cur_step()).reduce(0D, Double::sum) / (1024D * 1024 * 1024));

            sum_list.add(Arrays.stream(sat_data_amount).reduce(0D, Double::sum) / (1024D * 1024 * 1024));
            Double[] tmp = new Double[sat_data_amount.length];
            for (int i = 0; i < sat_data_amount.length; i++) {
                tmp[i] = ((double) sat_data_amount[i]) / (1024D * 1024 * 1024);
            }
            sat_data_amount_list.add(tmp);
            stations_active_sum += dto.getStations_active();
            Byte[] satelliteStatusesByQuant = new Byte[satellites.size()];

            for (int i = 0; i < satellites.size(); i++) {
                if (genetic_object.getD()[i] > 0) {
                    satelliteStatusesByQuant[i] = -1;
                } else if (genetic_object.getAc()[i] > 0) {
                    satelliteStatusesByQuant[i] = 1;
                } else {
                    satelliteStatusesByQuant[i] = 0;
                }
            }
            satelliteStatuses.add(satelliteStatusesByQuant);

            if (j % (100 * quant) == 0) {
                if (j != 0) {
                    System.out.print('\r');
                }
                System.out.print(j + "/" + quant_steps);
            }

        }
        for (int i = 0; i < satellites_cnt; i++) {
            not_full_satellites_list_duration_double[i] = (quant_steps - (full_satellites_list_duration_sum[i] / 1000D)) / 60D;
        }

        runtimeInfo.setAlgorithmEnd(System.currentTimeMillis());

        ReportDto report = ReportDto.builder()
                .quant(quant)
                .principle(d_principle)
                .countDays(countDays)
                .averageLoadInGb(sum_list.stream().mapToDouble(d -> d).average().orElse(0D) / 8D)
                .sumInGb(downloaded_data_list.stream().reduce(0D, Double::sum) / 8D)
                .minutesDuration(quant_steps / 60D)
                .fullSatellitesMinutes(Arrays.stream(full_satellites_list_duration_sum).mapToDouble(d -> d).sum() / 1000D / 60D)
                .stationsActivePercent(stations_active_sum * 100D / (((double) countDistinctStations) * quant_steps / quant))
                .build();

        PathHolder pathHolder = FileService.createResultsFolder(quant, d_principle, countDays);
        resultGenerator.generateGroundFiles(pathHolder);
        resultGenerator.generateCameraFiles(pathHolder);
        resultGenerator.generateDropFiles(pathHolder);
        resultGenerator.generateTransferByDayFile(pathHolder.getOurPath());
        new SumIntermediateGenerator().generateFile(pathHolder.getOurPath(), sum_list);
        new SumAmountIntermediateGenerator().generateFile(pathHolder.getOurPath(), sat_data_amount_list);
        new StatusByQuantIntermediateGenerator().generateFile(pathHolder.getOurPath(), satelliteStatuses, satellites);
        new NotFullSatellites().generateFile(pathHolder.getOurPath(), not_full_satellites_list_duration_double, satellites);

        runtimeInfo.setProgramEnd(System.currentTimeMillis());
        resultGenerator.generateReportFile(pathHolder.getOurPath(), report, runtimeInfo);

        System.out.println();
        System.out.println("end calculating");
        System.out.println("path to folder with results: " + pathHolder.getRootPath().toFile().getAbsolutePath());
    }
}
