package ru.khakhaton;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.stereotype.Service;
import ru.khakhaton.algorithm.Runner;
import ru.khakhaton.dto.RuntimeInfo;
import ru.khakhaton.parser.FacilityFileParser;
import ru.khakhaton.parser.IParser;
import ru.khakhaton.parser.RussiaFileParser;
import ru.khakhaton.service.FlyOverRussiaService;
import ru.khakhaton.service.FlyOverStationService;
import ru.khakhaton.service.SatelliteService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.stream.Stream;

@Slf4j
@Service
@RequiredArgsConstructor
public class ApplicationCommandLineRunner implements CommandLineRunner, ExitCodeGenerator {

    private static final String OPTION_F = "f";
    private static final String OPTION_Q = "q";
    private static final String OPTION_P = "p";
    private static final String OPTION_C = "c";
    private static final String RUSSIA_FOLDER = "Russia";

    private final FlyOverRussiaService flyOverRussiaService;
    private final FlyOverStationService flyOverStationService;
    private final SatelliteService satelliteService;
    private final Runner runner;
    private int exitCode;

    private Options options = new Options();
    private HelpFormatter formatter = new HelpFormatter();

    @Override
    public int getExitCode() {
        return exitCode;
    }

    @PostConstruct
    public void createCommandLineUtils() {
        Option folder = new Option(OPTION_F, "folder", true, "Pass two folders with files. Required");
        folder.setRequired(true);
        options.addOption(folder);

        Option quant = new Option(OPTION_Q, "quant", true, "Calculation step. 1-256. Default value: 64");
        options.addOption(quant);

        Option principle = new Option(OPTION_P, "principle", true, "Calculation method. Default value: 3");
        options.addOption(principle);

        Option countDays = new Option(OPTION_C, "count_days", true, "Count days for calculation. Default value: 13");
        options.addOption(countDays);
    }

    @Override
    public void run(String... args) {
        try {
            RuntimeInfo runtimeInfo = new RuntimeInfo();
            Set<String> folders = new HashSet<>();
            int quant = 64;
            int principle = 3;
            int countDays = 13;
            try {
                CommandLine commandLineParser = new DefaultParser().parse(options, args);

                for (Option option : commandLineParser.getOptions()) {
                    if (option.getOpt().equals(OPTION_F)) {
                        folders.addAll(option.getValuesList());
                    }
                    else if (option.getOpt().equals(OPTION_Q)) {
                        quant = Integer.parseInt(option.getValue());
                    }
                    else if (option.getOpt().equals(OPTION_P)) {
                        principle = Integer.parseInt(option.getValue());
                    }
                    else if (option.getOpt().equals(OPTION_C)) {
                        countDays = Integer.parseInt(option.getValue());
                    }
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
                formatter.printHelp("options", options);
                return ;
            }

            System.out.println("start parse files");
            doParse(folders);
            runtimeInfo.setParsingEnd(System.currentTimeMillis());
            System.out.println("end parse files");
            System.out.println("start calculating");
            runner.start(quant, principle, countDays, runtimeInfo);
        } catch (Exception ex) {
            log.error("error while application running.", ex);
            exitCode = 1;
        }
    }

    private void doParse(Set<String> folders) {
        folders.forEach(folder -> {
            File[] files = Objects.requireNonNull(new File(folder).listFiles());
            boolean isRussiaFolder = StringUtils.containsIgnoreCase(folder, RUSSIA_FOLDER);

            Stream.of(files)
                    .forEach(file -> {
                        IParser currentParser;

                        if (isRussiaFolder) {
                            currentParser = new RussiaFileParser(satelliteService, flyOverRussiaService);
                        } else {
                            currentParser = new FacilityFileParser(satelliteService, flyOverStationService);
                        }
                        currentParser.parseFileAndSaveData(file);
                    });
        });
    }
}
