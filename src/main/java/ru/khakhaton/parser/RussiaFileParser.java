package ru.khakhaton.parser;

import org.apache.commons.lang3.StringUtils;
import ru.khakhaton.dto.FlyOverRussiaDto;
import ru.khakhaton.dto.FlyOverStationDto;
import ru.khakhaton.service.FlyOverRussiaService;
import ru.khakhaton.service.SatelliteService;

import java.io.IOException;

import static ch.qos.logback.core.CoreConstants.EMPTY_STRING;

public class RussiaFileParser extends AbstractParser<FlyOverRussiaDto> {

    private final FlyOverRussiaService flyOverRussiaService;
    private boolean isStartOfBody = false;
    private boolean isExistsBody = false;

    public RussiaFileParser(SatelliteService satelliteService, FlyOverRussiaService flyOverRussiaService) {
        super(satelliteService);
        this.flyOverRussiaService = flyOverRussiaService;
    }

    @Override
    protected String getNextLineIfNeed() throws IOException {
        String line = EMPTY_STRING;
        if (!isStartOfBody) {
            line = readLine();
            isExistsBody = StringUtils.isNotBlank(line);
        }
        isStartOfBody = true;
        return line;
    }

    @Override
    protected String detectForFacilityFiles(String currentLine) {
        return StringUtils.stripStart(currentLine
                .substring(currentLine.length() - 4, currentLine.length() - 2), "0");
    }

    @Override
    protected boolean needSkipOneLine() {
        if (isExistsBody) {
            return true;
        } else {
            isExistsBody = true;
            return false;
        }
    }

    @Override
    protected FlyOverRussiaDto createDto(String stationOrPlain) {
        return new FlyOverRussiaDto().setPlain(stationOrPlain);
    }

    @Override
    protected void saveFlyOvers() {
        flyOverRussiaService.saveAll(mainHolderMap);
    }
}
