package ru.khakhaton.parser;

import ru.khakhaton.dto.FlyOverStationDto;
import ru.khakhaton.service.FlyOverStationService;
import ru.khakhaton.service.SatelliteService;

import java.io.IOException;

import static ch.qos.logback.core.CoreConstants.EMPTY_STRING;

public class FacilityFileParser extends AbstractParser<FlyOverStationDto> {

    private final FlyOverStationService flyOverStationService;

    public FacilityFileParser(SatelliteService satelliteService, FlyOverStationService flyOverStationService) {
        super(satelliteService);
        this.flyOverStationService = flyOverStationService;
    }

    @Override
    protected String getNextLineIfNeed() throws IOException {
        return readLine();
    }

    @Override
    protected String detectForFacilityFiles(String currentLine) {
        return stationForFacilityFiles;
    }

    @Override
    protected boolean needSkipOneLine() {
        return true;
    }

    @Override
    protected FlyOverStationDto createDto(String station) {
        return new FlyOverStationDto().setStation(station);
    }

    @Override
    protected void saveFlyOvers() {
        flyOverStationService.saveAll(mainHolderMap);
    }
}
