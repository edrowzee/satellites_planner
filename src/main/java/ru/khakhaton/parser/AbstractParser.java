package ru.khakhaton.parser;

import org.apache.commons.lang3.StringUtils;
import ru.khakhaton.dto.AbstractFlyOverDto;
import ru.khakhaton.exceptions.ParserException;
import ru.khakhaton.service.SatelliteService;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractParser<D extends AbstractFlyOverDto> implements IParser {

    private static final String README_FILENAME = "readme";
    private static final String FILECONST_TO = "-To-";
    private static final String FILECONST_ACCESS_SUMMARY_REPORT = ":  Access Summary Report";
    private static final String FILECONST_GLOBAL_STATISTICS = "Global Statistics";
    private static final String COMMA = ",";
    public static final DateTimeFormatter DATE_TIME_FORMATTER =
            DateTimeFormatter.ofPattern("d MMM yyyy HH:mm:ss.SSS", Locale.ENGLISH);

    private final SatelliteService satelliteService;
    protected BufferedReader reader;
    protected Map<String, List<D>> mainHolderMap = new HashMap<>();
    protected String stationForFacilityFiles;
    private int lineNumber = 0;

    protected AbstractParser(SatelliteService satelliteService) {
        this.satelliteService = satelliteService;
    }

    public void parseFileAndSaveData(File file) {
        String filename = file.getName();

        if (StringUtils.containsIgnoreCase(filename, README_FILENAME)) {
            return;
        }
        try {
            doParsingFile(file);
        } catch (Exception e) {
            throw new ParserException(lineNumber, filename, e);
        }
        satelliteService.saveAll(mainHolderMap.keySet());
        saveFlyOvers();
    }

    private void doParsingFile(File file) {
        try(Reader fileReader = new FileReader(file.getAbsoluteFile());
            BufferedReader reader = new BufferedReader(fileReader)) {
            this.reader = reader;

            parseHeader();
            parseBody();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void parseHeader() throws IOException {
        skipNLines(2);
        String line = readLine() + getNextLineIfNeed();
        String[] splitedString = line.split(FILECONST_TO);
        stationForFacilityFiles = splitedString[0];
        String allTitles = stripEndOfAllTitles(splitedString[1]);
        List<String> titles = Arrays.asList(allTitles.split(COMMA));

        mainHolderMap = titles.stream()
                .map(String::trim)
                .collect(Collectors.toMap(Function.identity(), tmp -> new ArrayList<>()));
    }

    private String stripEndOfAllTitles(String allTitles) {
        return allTitles.substring(0, allTitles.indexOf(FILECONST_ACCESS_SUMMARY_REPORT));
    }

    private void parseBody() throws IOException {
        while (true) {
            if (needSkipOneLine()) {
                skipOneLine();
            }
            String line = readLine();

            if (StringUtils.containsIgnoreCase(line, FILECONST_GLOBAL_STATISTICS)) {
                skipNLines(5);
                break;
            }

            line = readLine();
            String name = line.split(FILECONST_TO)[1];
            String plainOrStation = detectForFacilityFiles(line);
            skipNLines(3);

            for (line = readLine(); StringUtils.isNotBlank(line); line = readLine()) {
                String access = line.substring(20, 24).trim();
                String startTime = line.substring(28, 52).trim();
                String endTime = line.substring(56, 80).trim();
                String duration = line.substring(90, 98).trim();

                D dto = createDto(plainOrStation);
                dto.setSatellite(name)
                        .setAccess(Integer.parseInt(access))
                        .setStartTime(LocalDateTime.parse(startTime, DATE_TIME_FORMATTER))
                        .setEndTime(LocalDateTime.parse(endTime, DATE_TIME_FORMATTER))
                        .setDuration(duration);

                mainHolderMap.get("Satellite-" + name).add(dto);
            }
            skipNLines(3);
            line = readLine();
            if (line == null) {
                break;
            }
        }
    }

    protected abstract String getNextLineIfNeed() throws IOException;

    protected abstract String detectForFacilityFiles(String currentLine);

    protected abstract boolean needSkipOneLine();

    protected abstract D createDto(String stationOrPlain);

    private void skipNLines(int countToSkip) throws IOException {
        for (int i = 0; i < countToSkip; i++) {
            skipOneLine();
        }
    }

    private void skipOneLine() throws IOException {
        readLine();
    }

    protected String readLine() throws IOException {
        String line = reader.readLine();
        ++this.lineNumber;
        return line;
    }

    protected abstract void saveFlyOvers();
}
