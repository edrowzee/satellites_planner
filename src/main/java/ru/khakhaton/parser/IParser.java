package ru.khakhaton.parser;

import java.io.File;

public interface IParser {

    void parseFileAndSaveData(File file);
}
