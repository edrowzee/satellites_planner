package ru.khakhaton.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ru.khakhaton.entity.SatelliteEntity;
import ru.khakhaton.entity.enums.SatelliteType;
import ru.khakhaton.repository.SatelliteRepository;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SatelliteService {

    private final SatelliteRepository repository;

    @Transactional
    public void saveAll(Set<String> satellitesName) {
        List<SatelliteEntity> satellites = repository.findAll();
        Set<String> existsSatelliteNames = satellites.stream()
                .map(SatelliteEntity::getId)
                .collect(Collectors.toSet());
        Set<String> nonExistsSatelliteNames = satellitesName.stream()
                .filter(satellite -> !existsSatelliteNames.contains(satellite))
                .collect(Collectors.toSet());

        if (!CollectionUtils.isEmpty(nonExistsSatelliteNames)) {
            repository.saveAll(nonExistsSatelliteNames.stream()
                    .map(satellite -> SatelliteEntity.builder()
                            .id(satellite.split("Satellite-")[1])
                            .satelliteTypeId(detectSatelliteType(satellite))
                            .build())
                    .collect(Collectors.toList()));
        }
    }

    private Long detectSatelliteType(String satellite) {
        String props = satellite.split("_")[1];
        Integer scenarioVersion = Integer.parseInt(props.substring(0, 2));
        Integer planeNumber = Integer.parseInt(props.substring(2, 4));
        Integer satellitePlaneNumber = Integer.parseInt(props.substring(4, 6));

        return (planeNumber <= 5 ? SatelliteType.KINOSPUTNIK : SatelliteType.ZORKIY).getId();
    }
}
