package ru.khakhaton.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khakhaton.dto.FlyOverStationDto;
import ru.khakhaton.entity.FlyOverStationEntity;
import ru.khakhaton.mapper.FlyOverStationMapper;
import ru.khakhaton.repository.FlyOverStationRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class FlyOverStationService {

    private final FlyOverStationMapper mapper;
    private final FlyOverStationRepository repository;

    @Transactional
    public void saveAll(Map<String, List<FlyOverStationDto>> stationMap) {
        List<FlyOverStationEntity> entities = new ArrayList<>();

        stationMap.values().forEach(dtos ->
                dtos.forEach(dto -> entities.add(mapper.toEntity(dto))));

        repository.saveAll(entities);
    }
}
