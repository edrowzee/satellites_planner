package ru.khakhaton.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khakhaton.entity.SatelliteTypeEntity;
import ru.khakhaton.repository.SatelliteTypeRepository;

import java.math.BigDecimal;
import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class SatelliteTypeService {

    private final SatelliteTypeRepository repository;

    @EventListener(ContextRefreshedEvent.class)
    @Transactional
    public void contextRefreshedEvent() {
        repository.saveAll(Arrays.asList(
                SatelliteTypeEntity.builder()
                        .id(1L)
                        .name("Киноспутник")
                        .inputSpeed(new BigDecimal("4"))
                        .outputSpeed(new BigDecimal("1"))
                        .memory(new BigDecimal("1"))
                        .build(),
                SatelliteTypeEntity.builder()
                        .id(2L)
                        .name("Зоркий")
                        .inputSpeed(new BigDecimal("4"))
                        .outputSpeed(new BigDecimal("0.25"))
                        .memory(new BigDecimal("0.5"))
                        .build()
        ));
    }
}
