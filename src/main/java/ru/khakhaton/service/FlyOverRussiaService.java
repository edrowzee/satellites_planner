package ru.khakhaton.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.khakhaton.dto.FlyOverRussiaDto;
import ru.khakhaton.entity.FlyOverRussiaEntity;
import ru.khakhaton.mapper.FlyOverRussiaMapper;
import ru.khakhaton.repository.FlyOverRussiaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class FlyOverRussiaService {

    private final FlyOverRussiaMapper mapper;
    private final FlyOverRussiaRepository repository;

    @Transactional
    public void saveAll(Map<String, List<FlyOverRussiaDto>> stationMap) {
        List<FlyOverRussiaEntity> entities = new ArrayList<>();

        stationMap.values().forEach(dtos ->
                dtos.forEach(dto -> entities.add(mapper.toEntity(dto))));

        repository.saveAll(entities);
    }
}
