package ru.khakhaton.service;

import org.apache.commons.io.FileUtils;
import ru.khakhaton.dto.PathHolder;
import ru.khakhaton.generator.AbstractIntermediateGenerator;
import ru.khakhaton.generator.ResultGenerator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FileService {

    private static final String RESULTS_FOLDER_ROOR = "Results";
    private static final String CAMERA_FOLDER = "Camera";
    private static final String DROP_FOLDER = "Drop";
    private static final String GROUND_FOLDER = "Ground";

    public static PathHolder createResultsFolder(int quant, int principle, int countDays) {
        File rootFolder = Path.of(RESULTS_FOLDER_ROOR).toFile();
        if (!rootFolder.exists()) {
            rootFolder.mkdir();
        }
        String folderName = String.format("%s_quant-%d_principle-%d_days-%d",
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss").format(LocalDateTime.now()),
                quant, principle, countDays);
        File folder = Path.of(RESULTS_FOLDER_ROOR, folderName).toFile();

        folder.mkdirs();

        Path cameraPath = rootFolder.toPath().resolve(CAMERA_FOLDER);
        deleteIfExists(cameraPath);
        cameraPath.toFile().mkdirs();
        Path dropPath = rootFolder.toPath().resolve(DROP_FOLDER);
        deleteIfExists(dropPath);
        dropPath.toFile().mkdirs();
        Path groundPath = rootFolder.toPath().resolve(GROUND_FOLDER);
        deleteIfExists(groundPath);
        groundPath.toFile().mkdirs();

        return PathHolder.builder()
                .rootPath(rootFolder.toPath())
                .ourPath(folder.toPath())
                .cameraPath(cameraPath)
                .dropPath(dropPath)
                .groundPath(groundPath)
                .build();
    }

    private static void deleteIfExists(Path path) {
        try {
            FileUtils.deleteDirectory(path.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
