package ru.khakhaton.mapper;

import org.mapstruct.Mapper;
import ru.khakhaton.entity.FlyOverStationEntity;
import ru.khakhaton.dto.FlyOverStationDto;

@Mapper(componentModel = "spring")
public interface FlyOverStationMapper {

    FlyOverStationEntity toEntity(FlyOverStationDto dto);
}
