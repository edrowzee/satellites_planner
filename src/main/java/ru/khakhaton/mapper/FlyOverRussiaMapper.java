package ru.khakhaton.mapper;

import org.mapstruct.Mapper;
import ru.khakhaton.entity.FlyOverRussiaEntity;
import ru.khakhaton.dto.FlyOverRussiaDto;

@Mapper(componentModel = "spring")
public interface FlyOverRussiaMapper {

    FlyOverRussiaEntity toEntity(FlyOverRussiaDto dto);
}
