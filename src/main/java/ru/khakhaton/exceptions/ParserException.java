package ru.khakhaton.exceptions;


public class ParserException extends RuntimeException {

    public ParserException(int lineNumber, String filename, Throwable cause) {
        super(String.format("Error while parsing file: %s. Line number: %s", filename, lineNumber), cause);
    }
}
