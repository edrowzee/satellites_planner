package ru.khakhaton.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khakhaton.entity.FlyOverStationEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface FlyOverStationRepository extends JpaRepository<FlyOverStationEntity, Long> {

    @Query("select min(e.startTime) from FlyOverStationEntity e")
    LocalDateTime findMinStartTime();

    @Query("select max(e.endTime) from FlyOverStationEntity e")
    LocalDateTime findMaxEndTime();

    @Query("select count(distinct e.station) from FlyOverStationEntity e")
    long countDistinctStations();

    @Query(value = "SELECT * FROM (SELECT * FROM fly_over_station WHERE start_time BETWEEN " +
            " :window_start_time_str AND :window_end_time_str UNION " +
            "SELECT * FROM fly_over_station WHERE end_time BETWEEN " +
            " :window_start_time_str AND :window_end_time_str UNION " +
            "SELECT * FROM fly_over_station WHERE (:window_start_time_str" +
            " BETWEEN start_time AND end_time) AND (:window_end_time_str BETWEEN start_time AND end_time)" +
            " ORDER BY id) s ORDER BY s.id", nativeQuery = true)
    List<FlyOverStationEntity> findByDates(@Param("window_start_time_str") LocalDateTime window_start_time_str,
                                           @Param("window_end_time_str") LocalDateTime window_end_time_str);
}
