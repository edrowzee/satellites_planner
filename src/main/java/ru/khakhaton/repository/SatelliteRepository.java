package ru.khakhaton.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.khakhaton.entity.SatelliteEntity;

import java.util.List;

public interface SatelliteRepository extends JpaRepository<SatelliteEntity, String> {

    @Query("select s from SatelliteEntity s inner join fetch s.satelliteType order by s.id")
    List<SatelliteEntity> findAllWithJoinType();
}
