package ru.khakhaton.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khakhaton.entity.SatelliteTypeEntity;

public interface SatelliteTypeRepository extends JpaRepository<SatelliteTypeEntity, Long> {
}
