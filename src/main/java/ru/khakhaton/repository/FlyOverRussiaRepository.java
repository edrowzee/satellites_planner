package ru.khakhaton.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khakhaton.entity.FlyOverRussiaEntity;
import ru.khakhaton.entity.FlyOverStationEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface FlyOverRussiaRepository extends JpaRepository<FlyOverRussiaEntity, Long> {

    @Query("select min(e.startTime) from FlyOverRussiaEntity e")
    LocalDateTime findMinStartTime();

    @Query("select max(e.endTime) from FlyOverRussiaEntity e")
    LocalDateTime findMaxEndTime();

    @Query(value = "SELECT * FROM (SELECT * FROM fly_over_russia WHERE start_time BETWEEN " +
            ":window_start_time_str AND :window_end_time_str UNION " +
            "SELECT * FROM fly_over_russia WHERE end_time BETWEEN " +
            ":window_start_time_str AND :window_end_time_str UNION " +
            "SELECT * FROM fly_over_russia WHERE (:window_start_time_str " +
            "BETWEEN start_time AND end_time) AND (:window_end_time_str BETWEEN start_time AND end_time)" +
            " ORDER BY id) s ORDER BY s.id", nativeQuery = true)
    List<FlyOverRussiaEntity> findByDates(@Param("window_start_time_str") LocalDateTime window_start_time_str,
                                          @Param("window_end_time_str") LocalDateTime window_end_time_str);
}
