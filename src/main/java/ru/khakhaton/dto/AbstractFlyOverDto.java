package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AbstractFlyOverDto {
    private String satellite;
    private int access;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String duration;
}
