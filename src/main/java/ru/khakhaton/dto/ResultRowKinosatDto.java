package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultRowKinosatDto {

    private long access;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String stationName;
    private double data;

    public long getDuration() {
        return Duration.between(startTime, endTime).toMillis();
    }
}
