package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.file.Path;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PathHolder {

    private Path rootPath;
    private Path ourPath;
    private Path cameraPath;
    private Path dropPath;
    private Path groundPath;
}
