package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CalcSumVectorDto {
    private double[] sum_cur_step;
    private double[] downloaded_cur_step;
    private long stations_active;
    private long[] full_satellites_list_duration_sum;
}
