package ru.khakhaton.dto;

import lombok.Data;

@Data
public class RuntimeInfo {

    private long startProgram;
    private long parsingEnd;
    private long algorithmEnd;
    private long programEnd;

    public RuntimeInfo() {
        startProgram = System.currentTimeMillis();
    }
}
