package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportDto {

    private int quant;
    private int principle;
    private int countDays;
    private double averageLoadInGb;
    private double sumInGb;
    private double minutesDuration;
    private double fullSatellitesMinutes;
    private double stationsActivePercent;
}
