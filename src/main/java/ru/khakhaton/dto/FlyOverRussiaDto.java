package ru.khakhaton.dto;

import lombok.*;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FlyOverRussiaDto extends AbstractFlyOverDto {

    private String plain;
}