package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultRowDto {

    private long access;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String satelliteName;
    private double data;

    public void concatWithRow(ResultRowDto anotherRow) {
        endTime = anotherRow.getEndTime();
        data += anotherRow.getData();
    }

    public long getDuration() {
        return Duration.between(startTime, endTime).toMillis();
    }
}
