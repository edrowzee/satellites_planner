package ru.khakhaton.dto;

import lombok.*;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FlyOverStationDto extends AbstractFlyOverDto {

    private String station;
}
