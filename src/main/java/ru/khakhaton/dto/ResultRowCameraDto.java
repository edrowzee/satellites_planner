package ru.khakhaton.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultRowCameraDto {

    private long access;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private double data;
    private boolean stopSum;
    private boolean newRow;

    public void concatWithRow(ResultRowCameraDto anotherRow) {
        endTime = anotherRow.getEndTime();
        data += anotherRow.getData();
    }

    public long getDuration() {
        return Duration.between(startTime, endTime).toMillis();
    }
}