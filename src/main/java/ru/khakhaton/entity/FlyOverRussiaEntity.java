package ru.khakhaton.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@Table(name = "fly_over_russia")
@EntityListeners(AuditingEntityListener.class)
public class FlyOverRussiaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqid-gen")
    @SequenceGenerator(name = "seqid-gen", sequenceName = "fly_over_russia_SEQ", allocationSize = 1, initialValue = 0)
    private Long id;

    @Column(name = "plain", nullable = false)
    private String plain;

    @Column(name = "satellite", nullable = false)
    private String satellite;

    @Column(name = "access", nullable = false)
    private int access;

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;

    @Column(name = "duration", nullable = false)
    private String duration;
}