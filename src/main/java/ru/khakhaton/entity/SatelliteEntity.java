package ru.khakhaton.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ru.khakhaton.entity.enums.SatelliteType;

import javax.persistence.*;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@Table(name = "satellites")
@EntityListeners(AuditingEntityListener.class)
public class SatelliteEntity {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "satellite_type_id", nullable = false)
    private Long satelliteTypeId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "satellite_type_id", referencedColumnName = "id", nullable = false, updatable = false, insertable = false)
    private SatelliteTypeEntity satelliteType;
}
