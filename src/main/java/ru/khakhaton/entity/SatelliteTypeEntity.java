package ru.khakhaton.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
@Table(name = "satellite_type")
@EntityListeners(AuditingEntityListener.class)
public class SatelliteTypeEntity {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "input_speed", nullable = false)
    private BigDecimal inputSpeed;

    @Column(name = "output_speed", nullable = false)
    private BigDecimal outputSpeed;

    @Column(name = "memory", nullable = false)
    private BigDecimal memory;
}
