package ru.khakhaton.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SatelliteType {

    KINOSPUTNIK(1L),
    ZORKIY(2L);

    private final Long id;
}
