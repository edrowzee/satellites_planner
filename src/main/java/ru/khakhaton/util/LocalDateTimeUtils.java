package ru.khakhaton.util;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;

@UtilityClass
public class LocalDateTimeUtils {

    public static LocalDateTime stripMillis(LocalDateTime localDateTime) {
        return localDateTime.withNano(0);
    }
}
